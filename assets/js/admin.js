require('../css/admin.css');
require('../scss/admin.scss');
require('../css/material-icons-outline-master/material-icons.css');
import '../js/preload/preload.js'
const $ = require('jquery');


// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');
// or you can include specific pieces
 require('bootstrap/js/dist/tooltip');
 require('bootstrap/js/dist/popover');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
//Cacher menu vers la gauche
document.getElementById("sidebar-drop-right").addEventListener("click", cacherSidebard);
function cacherSidebard() {
    var e = document.getElementById("nav-sidebar");
    if(e.className =="sidebar sidebar-sticky d-none d-md-none d-lg-block"){
        e.classList.add("togged");
    }else{
        e.classList.remove("togged");
    }
}
//partie datable
require( 'datatables.net' )( window, $ );
require( 'datatables.net-bs4/css/dataTables.bootstrap4.min.css' );
require( 'datatables.net-bs4/js/dataTables.bootstrap4.min.js' );
require( 'jszip' )( window, $ );
//require( 'pdfmake' );
require( 'datatables.net-autofill-bs4' )( window, $ );
require( 'datatables.net-buttons-bs4' )( window, $ );
require( 'datatables.net-buttons/js/buttons.colVis.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.flash.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.html5.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.print.js' )( window, $ );
require( 'datatables.net-colreorder-bs4' )( window, $ );
require( 'datatables.net-fixedcolumns-bs4' )( window, $ );
require( 'datatables.net-fixedheader-bs4' )( window, $ );
require( 'datatables.net-keytable-bs4' )( window, $ );
require( 'datatables.net-responsive-bs4' )( window, $ );
require( 'datatables.net-rowgroup-bs4' )( window, $ );
require( 'datatables.net-rowreorder-bs4' )( window, $ );
require( 'datatables.net-scroller-bs4' )( window, $ );
require( 'datatables.net-select-bs4' )( window, $ );
$(document).ready( function () {
    if($('html')[0].lang === "fr_FR"){

        $('#datatables-dashboard').DataTable({
            pageLength: 10,
            lengthChange: false,
            bFilter: false,
            autoWidth: false,
            responsive: true,
            language:{
                "sProcessing":     "Traitement en cours...",
                "sSearch":         "Rechercher&nbsp;:",
                "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix":    "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst":      "Premier",
                    "sPrevious":   "Pr&eacute;c&eacute;dent",
                    "sNext":       "Suivant",
                    "sLast":       "Dernier"
                },
                "oAria": {
                    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                },
                "select": {
                    "rows": {
                        _: "%d lignes séléctionnées",
                        0: "Aucune ligne séléctionnée",
                        1: "1 ligne séléctionnée"
                    }
                }
            }
        });
    }else{
        $('#datatables-dashboard').DataTable({
            pageLength: 10,
            lengthChange: false,
            bFilter: false,
            autoWidth: false,
            responsive: true
        });
    }


} );

// shart Js
var Chart = require('chart.js');

new Chart(document.getElementById("line-chart"), {
    type: 'line',
    data: {
        labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
        datasets: [{
            data: [86,114,106,106,107,111,133,221,783,2478],
            label: "Africa",
            borderColor: "#3e95cd",
            fill: false
        }, {
            data: [282,350,411,502,635,809,947,1402,3700,5267],
            label: "Asia",
            borderColor: "#8e5ea2",
            fill: false
        }, {
            data: [168,170,178,190,203,276,408,547,675,734],
            label: "Europe",
            borderColor: "#3cba9f",
            fill: false
        }, {
            data: [40,20,10,16,24,38,74,167,508,784],
            label: "Latin America",
            borderColor: "#e8c3b9",
            fill: false
        }, {
            data: [6,3,2,2,7,26,82,172,312,433],
            label: "North America",
            borderColor: "#c45850",
            fill: false
        }
        ]
    },
    options: {
        title: {
            display: true,
            text: 'World population per region (in millions)'
        }
    }
});

//pignos calandar
require('pg-calendar/dist/css/pignose.calendar.css');
require('pg-calendar/dist/js/pignose.calendar.js');
$(function() {
    $('.calendar').pignoseCalendar(
        {
            lang: 'fr'
        }

    );
});
