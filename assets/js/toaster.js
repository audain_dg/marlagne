import toastr from 'toastr';

//Succes Message;
function sucessAlert(message){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "600",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr["success"](message);
}
document.addEventListener('DOMContentLoaded', function() {
    let toastSuccessMessage = $('.toast-alert-success').data('msg');
    let toastErrorMessage = $('.toast-alert-error').data('msg');
    if(typeof  toastSuccessMessage  !== "undefined"){
        sucessAlert(toastSuccessMessage);
    }
    if(typeof  toastErrorMessage  !== "undefined"){
        errorAlert(toastErrorMessage);
    }
});
function errorAlert(message){
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "600",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr["error"](message);
}
export {errorAlert, sucessAlert};
