import places from 'places.js';
let language=  $('html')[0].lang.substr(0, 2) ;
(function() {
    var placesAutocomplete = places({
        appId: 'plCMF943223P',
        apiKey: '21fc70074a12c9c4231721349e3af473',
        container: document.querySelector('#street'),
        templates: {
            value: function(suggestion) {
                return suggestion.name;
            }
        }
    }).configure({
        language: language,
        type: 'address'
    });
    placesAutocomplete.on('change', function resultSelected(e) {
        document.querySelector('#country').value = e.suggestion.country || '';
        document.querySelector('#region').value = e.suggestion.administrative || '';
        document.querySelector('#city').value = e.suggestion.city || '';
        document.querySelector('#zipCode').value = e.suggestion.postcode || '';
    });
})();