import '../../../css/admin.css';
import '../../../scss/admin.scss';
import '../../../css/material-icons-outline-master/material-icons.css';
import '../../preload/preload.js';
import '../../menu/hidSideBar.js';
import $ from'jquery';
import {errorAlert,sucessAlert} from '../../../js/toaster';
import 'bootstrap';
import 'bootstrap/js/dist/tooltip';
import 'bootstrap/js/dist/popover';

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});


//partie datable
require( 'datatables.net' )( window, $ );
require( 'datatables.net-bs4/css/dataTables.bootstrap4.min.css' );
require( 'datatables.net-bs4/js/dataTables.bootstrap4.min.js' );
require( 'jszip' )( window, $ );
//require( 'pdfmake' );
require( 'datatables.net-autofill-bs4' )( window, $ );
require( 'datatables.net-buttons-bs4' )( window, $ );
require( 'datatables.net-buttons/js/buttons.colVis.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.flash.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.html5.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.print.js' )( window, $ );
require( 'datatables.net-colreorder-bs4' )( window, $ );
require( 'datatables.net-fixedcolumns-bs4' )( window, $ );
require( 'datatables.net-fixedheader-bs4' )( window, $ );
require( 'datatables.net-keytable-bs4' )( window, $ );
require( 'datatables.net-responsive-bs4' )( window, $ );
require( 'datatables.net-rowgroup-bs4' )( window, $ );
require( 'datatables.net-rowreorder-bs4' )( window, $ );
require( 'datatables.net-scroller-bs4' )( window, $ );
require( 'datatables.net-select-bs4' )( window, $ );
$(document).ready( function () {
    if($('html')[0].lang === "fr_FR"){

        $('#datatables-dashboard').DataTable({
            pageLength: 10,
            lengthChange: true,
            bFilter: false,
            autoWidth: false,
            responsive: true,
            language:{
                "sProcessing":     "Traitement en cours...",
                "sSearch":         "Rechercher&nbsp;:",
                "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix":    "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst":      "Premier",
                    "sPrevious":   "Pr&eacute;c&eacute;dent",
                    "sNext":       "Suivant",
                    "sLast":       "Dernier"
                },
                "oAria": {
                    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                },
                "select": {
                    "rows": {
                        _: "%d lignes séléctionnées",
                        0: "Aucune ligne séléctionnée",
                        1: "1 ligne séléctionnée"
                    }
                }
            }
        });
    }else{
        $('#datatables-dashboard').DataTable({
            pageLength: 10,
            lengthChange: false,
            bFilter: false,
            autoWidth: false,
            responsive: true
        });
    }


} );
// PARTIE SELECT 2
import select2 from 'select2';
import 'select2/dist/css/select2.css';
select2($);
$(document).ready(function() {
    $('#user_info_public_Country').select2();
    $('#user_info_public_City').select2();
    $('#user_info_public_Region').select2();

});
// PARTIE SELECT CONTRY - REGION - CITY
$(document).on('change', '#user_info_public_Country, #user_info_public_Region', function () {
    let $field = $(this);
    let $form = $field.closest('form');
    let data =$form.serialize();
    $.post($form.attr('action'), data).then(function(data) {
        let inputRegion = $(data).find("select#user_info_public_Region");
        let inputVille = $(data).find("select#user_info_public_City");
        $("select#user_info_public_Region").replaceWith(inputRegion);
        $("select#user_info_public_City").replaceWith(inputVille);
        $('#user_info_public_Country').select2();
        $('#user_info_public_City').select2();
        $('#user_info_public_Region').select2();
    });
});
// PARTIE AVATAR
$("#bodyImageAvatar,#UploadAvatarIcon").bind( "click", function() {
    document.getElementById("user_info_public_avatar").click();
    //$( "#textImageAvatar" ).replaceWith( $( "<span id=\"textImageAvatar\">Modifier</span>" ) );
});
function readURL(input) {
    let validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
    if (!validImageTypes.includes(input.files[0].type)) {
        let message;
        if($('html')[0].lang === "fr_FR"){
            message = "Format autorisé: gif, jpeg, png";
        }else{
            let message= "authorized format: gif, jpeg, png";
        }
        errorAlert(message);
        $("#user_new_avatar").val('');
    }else if(input.files && input.files[0]){
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#uploadAvatar').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }

}
$("#user_new_avatar").change(function () {
    readURL(this);
});

