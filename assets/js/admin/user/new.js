import '../../../css/admin.css';
import '../../../scss/admin.scss';
import '../../../css/material-icons-outline-master/material-icons.css';
import '../../preload/preload.js';
import '../../menu/hidSideBar.js';
import '../../algolia/places.js';
import $ from'jquery';
import {errorAlert,sucessAlert} from '../../../js/toaster';
import 'bootstrap';
import 'bootstrap/js/dist/tooltip';
import 'bootstrap/js/dist/popover';
$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
//PWD STRENGH
import '../../passwordStrength/strengthify.js';
import '../../passwordStrength/strengthify.css';
$('#user_new_plainPassword_first').strengthify({
    zxcvbn: 'https://cdn.rawgit.com/dropbox/zxcvbn/master/dist/zxcvbn.js',
    drawTitles: true,
    drawMessage: false,
    onResult: function(result) {
        var submitBtn = $('#user_new_save');

        if (result.score < 3) {
            submitBtn.prop('disabled', 'disabled');
        } else {
            submitBtn.prop('disabled', false);
        }
    }
});
// PARTIE SELECT 2
import select2 from 'select2';
import 'select2/dist/css/select2.css';
select2($);
$(document).ready(function() {
    $('#user_new_roles').select2({width: '100%' });
});
// PARTIE AVATAR
$("#bodyImageAvatar,#UploadAvatarIcon").bind( "click", function() {
    document.getElementById("user_new_avatar").click();
});
function readURL(input) {
    let validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
    if (!validImageTypes.includes(input.files[0].type)) {
        let message;
        if($('html')[0].lang === "fr_FR"){
            message = "Format autorisé: gif, jpeg, png";
        }else{
            let message= "authorized format: gif, jpeg, png";
        }
        errorAlert(message);
        $("#user_new_avatar").val('');
    }else if(input.files && input.files[0]){
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#uploadAvatar').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }

}
$("#user_new_avatar").change(function () {
    readURL(this);
});

/* Verification integer input */
$('#user_new_StreetNumber,#user_new_ZipCode').keyup(function(e)
{
    if (/\D/g.test(this.value))
    {
        // Filter non-digits from input value.
        this.value = this.value.replace(/\D/g, '');
    }
});
/* add isvalid  */
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

/* select2 add isvalid */
$(".select").select2({
    minimumResultsForSearch: Infinity
});