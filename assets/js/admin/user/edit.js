import '../../../css/admin.css';
import '../../../scss/admin.scss';
import '../../../css/material-icons-outline-master/material-icons.css';
import '../../preload/preload.js';
import '../../menu/hidSideBar.js';
import $ from'jquery';
import {errorAlert,sucessAlert} from '../../../js/toaster';
import 'bootstrap';
import 'bootstrap/js/dist/tooltip';
import 'bootstrap/js/dist/popover';
import 'pg-calendar/dist/css/pignose.calendar.css';
import 'pg-calendar/dist/js/pignose.calendar.js';
$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});

// PARTIE SELECT 2
import select2 from 'select2';
import 'select2/dist/css/select2.css';
select2($);
$(document).ready(function() {
    $('#user_roles').select2({width: '100%' });

});
// PARTIE SELECT ENTITY

$(document).on('change', '#GeneralEntity', function () {
    let $field = $(this);
    let $form = $field.closest('form');
    let data =$form.serialize();
    $.post($form.attr('action'), data).then(function(data) {

        let inputLocalEntity = $(data).find("select#LocalEntity");

        $("select#LocalEntity").replaceWith(inputLocalEntity);
        $('#GeneralEntity').select2({width: '100%' });
       $('#LocalEntity').select2({width: '100%' });
    });
});

$(document).on('change', '#FormationType', function () {
    let $field = $(this);
    let $form = $field.closest('form');
    let data =$form.serialize();
    $.post($form.attr('action'), data).then(function(data) {

        let inputLocalEntity = $(data).find("select#Formation");

        $("select#Formation").replaceWith(inputLocalEntity);

    });
});



/* add isvalid  */
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

/* select2 add isvalid */
$(".select").select2({
    minimumResultsForSearch: Infinity
});
/* PIGNOS CALENDAR */
$(function() {
    $('#user_birthDate ').pignoseCalendar(
        {
            lang: 'fr',
            format: 'DD-MM-YYYY'
        }

    );
});


/* AJAX addQuotationDescription FORM*/
$(document).on('submit', '#FormationLigneForm', function () {


    let $form = $('#FormationLigneForm');
    let data =$form.serialize();

    $.post($form.attr('action'), data).then(function(data) {

        let result = '                        <div class="form-row">\n' +
            '                            <div class="form-group col-md-6">\n' +
            '                                <label class="col-form-label col-form-label-sm required" for="FormationType">FormationType</label>\n' +
            '                                <input type="text" id="FormationType" name="user[FormationType]" disabled="disabled" required="required" maxlength="255" value="'+$("#FormationType :selected").text()+'" class="form-control form-control-sm">\n' +
            '                            </div>\n' +
            '                            <div class="form-group col-md-6">\n' +
            '                                    <label class="col-form-label col-form-label-sm required" for="Formation">Formation</label>\n' +
            '                                    <input type="text" id="Formation" name="user[Formation]" disabled="disabled" required="required" maxlength="255" value="'+ $("#Formation :selected").text() +'" class="form-control form-control-sm">\n' +
            '                            </div> \n' +
            '                            <div class="row col-12 justify-content-center pt-3">\n' +
            '                                <a href="/admin/formation_ligne/'+$('#id-user').text()+'/'+$("#Formation :selected").val()+'" id="quotation_description_delete" class="btn btn-sm btn-danger col-12 col-lg-3">Supprimer</a>\n' +
            '                            </div>\n' +
            '                        </div>';
        $('#add_field').append('<hr>').append(result).append('<hr>');
        //$('#add_field>#save_field>div>select ,#add_field>#save_field>div>input').attr('disabled', 'disabled');
       // $('#add_field>#save_field>div>#quotation_description_delete').removeClass('d-none')
    }).fail(function(error){
        alert('I am an error'+ error);
    });
    return false;

});
$(document).on('submit', '#reservationForm', function () {


    let $form = $('#reservationForm');
    let data =$form.serialize();

    $.post($form.attr('action'), data).then(function(data) {

        let result = '                        <div class="form-row">\n' +
            '                            <div class="form-group col-md-3">\n' +
            '                                <label class="col-form-label col-form-label-sm required" for="FormationType">Date</label>\n' +
            '                                <input type="text" id="FormationType" name="user[FormationType]" disabled="disabled" required="required" maxlength="255" value="'+$("#Calendar :selected").text()+'" class="form-control form-control-sm">\n' +
            '                            </div>\n' +
            '                            <div class="form-group col-md-3">\n' +
            '                                    <label class="col-form-label col-form-label-sm required" for="Formation">Dinner</label>\n' +
            '                                    <input type="text" id="Formation" name="user[Formation]" disabled="disabled" required="required" maxlength="255" value="'+ $("#dinner :selected").text() +'" class="form-control form-control-sm">\n' +
            '                            </div> \n' +
            '                            <div class="form-group col-md-3">\n' +
            '                                    <label class="col-form-label col-form-label-sm required" for="Formation">Supper</label>\n' +
            '                                    <input type="text" id="Formation" name="user[Formation]" disabled="disabled" required="required" maxlength="255" value="'+ $("#supper :selected").text() +'" class="form-control form-control-sm">\n' +
            '                            </div> \n' +
            '                            <div class="form-group col-md-3">\n' +
            '                                    <label class="col-form-label col-form-label-sm required" for="Formation">Supper</label>\n' +
            '                                    <input type="text" id="Formation" name="user[Formation]" disabled="disabled" required="required" maxlength="255" value="'+ $("#logement :selected").text() +'" class="form-control form-control-sm">\n' +
            '                            </div> \n' +
            '                            <div class="row col-12 justify-content-center pt-3">\n' +
            '                                <a href="/admin/quotation/143/admin_quotation_description_delete" id="quotation_description_delete" class="btn btn-sm btn-danger col-12 col-lg-3">Supprimer</a>\n' +
            '                            </div>\n' +
            '                        </div>';
        $('#add_field_reservation').append('<hr>').append(result).append('<hr>');
        //$('#add_field>#save_field>div>select ,#add_field>#save_field>div>input').attr('disabled', 'disabled');
        // $('#add_field>#save_field>div>#quotation_description_delete').removeClass('d-none')
    }).fail(function(error){
        alert('I am an error'+ error);
    });
    return false;

});


/* Changer les valeurs  si LocalEntity et  GeneralEntity sont setté */
$('#GeneralEntity > option').each(function() {
   if($(this).text() === $('#value-general').text())
   {

       $('#GeneralEntity').val($(this).val()).trigger('click');

       $('#LocalEntity > option').each(function() {
           if($(this).text() === $('#value-local').text())
           {

               $('#LocalEntity').val($(this).val()).trigger('click');
           }
       });



   }
});

