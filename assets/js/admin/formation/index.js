import '../../../css/admin.css';
import '../../../scss/admin.scss';
import '../../../css/material-icons-outline-master/material-icons.css';
import '../../preload/preload.js';
import '../../menu/hidSideBar.js';
import $ from'jquery';
import {errorAlert,sucessAlert} from '../../../js/toaster';
import 'bootstrap';
import 'bootstrap/js/dist/tooltip';
import 'bootstrap/js/dist/popover';

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
//partie datable
require( 'datatables.net' )( window, $ );
require( 'datatables.net-bs4/css/dataTables.bootstrap4.min.css' );
require( 'datatables.net-bs4/js/dataTables.bootstrap4.min.js' );
require( 'jszip' )( window, $ );
//require( 'pdfmake' );
require( 'datatables.net-autofill-bs4' )( window, $ );
require( 'datatables.net-buttons-bs4' )( window, $ );
require( 'datatables.net-buttons/js/buttons.colVis.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.flash.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.html5.js' )( window, $ );
require( 'datatables.net-buttons/js/buttons.print.js' )( window, $ );
require( 'datatables.net-colreorder-bs4' )( window, $ );
require( 'datatables.net-fixedcolumns-bs4' )( window, $ );
require( 'datatables.net-fixedheader-bs4' )( window, $ );
require( 'datatables.net-keytable-bs4' )( window, $ );
require( 'datatables.net-responsive-bs4' )( window, $ );
require( 'datatables.net-rowgroup-bs4' )( window, $ );
require( 'datatables.net-rowreorder-bs4' )( window, $ );
require( 'datatables.net-scroller-bs4' )( window, $ );
require( 'datatables.net-select-bs4' )( window, $ );
$(document).ready( function () {
    if($('html')[0].lang === "fr_FR"){

        $('#datatables-dashboard').DataTable({
            pageLength: 10,
            lengthChange: true,
            bFilter: false,
            autoWidth: false,
            responsive: true,
            language:{
                "sProcessing":     "Traitement en cours...",
                "sSearch":         "Rechercher&nbsp;:",
                "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                "sInfo":           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                "sInfoEmpty":      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                "sInfoPostFix":    "",
                "sLoadingRecords": "Chargement en cours...",
                "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                "oPaginate": {
                    "sFirst":      "Premier",
                    "sPrevious":   "Pr&eacute;c&eacute;dent",
                    "sNext":       "Suivant",
                    "sLast":       "Dernier"
                },
                "oAria": {
                    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                },
                "select": {
                    "rows": {
                        _: "%d lignes séléctionnées",
                        0: "Aucune ligne séléctionnée",
                        1: "1 ligne séléctionnée"
                    }
                }
            }
        });
    }else{
        $('#datatables-dashboard').DataTable({
            pageLength: 10,
            lengthChange: false,
            bFilter: false,
            autoWidth: false,
            responsive: true
        });
    }


} );