import '../../../css/admin.css';
import '../../../scss/admin.scss';
import '../../../css/material-icons-outline-master/material-icons.css';
import '../../preload/preload.js';
import '../../menu/hidSideBar.js';
import $ from'jquery';
import {errorAlert,sucessAlert} from '../../../js/toaster';
import 'bootstrap';
import 'bootstrap/js/dist/tooltip';
import 'bootstrap/js/dist/popover';
import 'pg-calendar/dist/css/pignose.calendar.css';
import 'pg-calendar/dist/js/pignose.calendar.js';

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});
// PARTIE AVATAR
$("#bodyImageAvatar,#UploadAvatarIcon").bind( "click", function() {
    document.getElementById("category_image").click();
});
function readURL(input) {
    let validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
    if (!validImageTypes.includes(input.files[0].type)) {
        let message;
        if($('html')[0].lang === "fr_FR"){
            message = "Format autorisé: gif, jpeg, png";
        }else{
            let message= "authorized format: gif, jpeg, png";
        }
        errorAlert(message);
        $("#category_image").val('');
    }else if(input.files && input.files[0]){
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#uploadAvatar').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }

}
$("#category_image").change(function () {
    readURL(this);
});


/* ------------VERFICATION FORM----------*/

/* add isvalid  */
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

/* PIGNOS CALENDAR */
$(function() {
    $('#startDate,#endDate ').pignoseCalendar(
        {
            lang: $('html')[0].lang.substr(0, 2),
            format: 'DD-MM-YYYY'
        }

    );
});
