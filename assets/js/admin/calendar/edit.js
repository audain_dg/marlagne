import '../../../css/admin.css';
import '../../../scss/admin.scss';
import '../../../css/material-icons-outline-master/material-icons.css';
import '../../preload/preload.js';
import '../../menu/hidSideBar.js';
import $ from'jquery';
import {errorAlert,sucessAlert} from '../../../js/toaster';
import 'bootstrap';
import 'bootstrap/js/dist/tooltip';
import 'bootstrap/js/dist/popover';
import select2 from 'select2';
import 'select2/dist/css/select2.css';
import 'pg-calendar/dist/css/pignose.calendar.css';
import 'pg-calendar/dist/js/pignose.calendar.js';


$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});


/* add isvalid  */
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

/* PIGNOS CALENDAR */
$(function() {
    $('#calendar_date').pignoseCalendar(
        {
            lang: 'fr',
            format: 'DD-MM-YYYY'
        }

    );
});
