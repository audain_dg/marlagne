// Show / Hide Tables

function toggleTable1() {
  var a1 = document.getElementById("table_body1");
    if (a1.style.display === "block") {
      a1.style.display = "none";
      document.getElementById("show-hide1").innerHTML = "afficher";
    } else {
      a1.style.display = "block";
      document.getElementById("show-hide1").innerHTML = "masquer";
    }
}
function toggleTable2() {
  var a2 = document.getElementById("table_body2");
  if (a2.style.display === "block") {
    a2.style.display = "none";
    document.getElementById("show-hide2").innerHTML = "afficher";
  } else {
    a2.style.display = "block";
    document.getElementById("show-hide2").innerHTML = "masquer";
  }
}
function toggleTable3() {
  var a3 = document.getElementById("table_body3");
    if (a3.style.display === "block") {
      a3.style.display = "none";
      document.getElementById("show-hide3").innerHTML = "afficher";
    } else {
      a3.style.display = "block";
      document.getElementById("show-hide3").innerHTML = "masquer";
    }
}
function toggleTable4() {
  var a4 = document.getElementById("table_body4");
    if (a4.style.display === "block") {
      a4.style.display = "none";
      document.getElementById("show-hide4").innerHTML = "afficher";
    } else {
      a4.style.display = "block";
      document.getElementById("show-hide4").innerHTML = "masquer";
    }
}
function toggleTable5() {
  var a5 = document.getElementById("table_body5");
    if (a5.style.display === "block") {
      a5.style.display = "none";
      document.getElementById("show-hide5").innerHTML = "afficher";
    } else {
      a5.style.display = "block";
      document.getElementById("show-hide5").innerHTML = "masquer";
    }
}
function toggleTable6() {
  var a6 = document.getElementById("table_body6");
    if (a6.style.display === "block") {
      a6.style.display = "none";
      document.getElementById("show-hide6").innerHTML = "afficher";
    } else {
      a6.style.display = "block";
      document.getElementById("show-hide6").innerHTML = "masquer";
    }
}
function toggleTable7() {
  var a7 = document.getElementById("table_body7");
    if (a7.style.display === "block") {
      a7.style.display = "none";
      document.getElementById("show-hide7").innerHTML = "afficher";
    } else {
      a7.style.display = "block";
      document.getElementById("show-hide7").innerHTML = "masquer";
    }
}
function toggleTable8() {
  var a8 = document.getElementById("table_body8");
    if (a8.style.display === "block") {
      a8.style.display = "none";
      document.getElementById("show-hide8").innerHTML = "afficher";
    } else {
      a8.style.display = "block";
      document.getElementById("show-hide8").innerHTML = "masquer";
    }
}
function toggleTable9() {
  var a9 = document.getElementById("table_body9");
    if (a9.style.display === "block") {
      a9.style.display = "none";
      document.getElementById("show-hide9").innerHTML = "afficher";
    } else {
      a9.style.display = "block";
      document.getElementById("show-hide9").innerHTML = "masquer";
    }
}



// select all

function selectAll() {
  var items = document.getElementsByClassName('presence');
  for (var i = 0; i < items.length; i++) {
      if (items[i].type == 'checkbox')
          items[i].checked = true;
  }
}

function unselectAll() {
  var items = document.getElementsByClassName('presence');
  for (var i = 0; i < items.length; i++) {
      if (items[i].type == 'checkbox')
          items[i].checked = false;
  }
}


// Show/Hide Matricule Field

function HideMatricule() {
if (document.getElementById('volontaire').checked) {
  document.getElementById('matricule').style.display = "block";
} else {
  document.getElementById('matricule').style.display = "none";
}
}


// Enable Submit button

function EnableButton() {
  if (document.getElementById('agreementCB').checked) {
    document.getElementById("submitButton").classList.remove("disabled");
  } else {
    document.getElementById("submitButton").classList.add("disabled");
  }
}



