<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dinner;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $supper;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $logement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Calendar", inversedBy="reservations")
     */
    private $Calendar;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="reservations")
     */
    private $User;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDinner(): ?string
    {
        return $this->dinner;
    }

    public function setDinner(string $dinner): self
    {
        $this->dinner = $dinner;

        return $this;
    }

    public function getSupper(): ?string
    {
        return $this->supper;
    }

    public function setSupper(string $supper): self
    {
        $this->supper = $supper;

        return $this;
    }

    public function getLogement(): ?string
    {
        return $this->logement;
    }

    public function setLogement(string $logement): self
    {
        $this->logement = $logement;

        return $this;
    }

    public function getCalendar(): ?Calendar
    {
        return $this->Calendar;
    }

    public function setCalendar(?Calendar $Calendar): self
    {
        $this->Calendar = $Calendar;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }
}
