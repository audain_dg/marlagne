<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GeneralEntityRepository")
 */
class GeneralEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LocalEntity", mappedBy="GeneralEntity")
     */
    private $localEntities;

    public function __construct()
    {
        $this->localEntities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|LocalEntity[]
     */
    public function getLocalEntities(): Collection
    {
        return $this->localEntities;
    }

    public function addLocalEntity(LocalEntity $localEntity): self
    {
        if (!$this->localEntities->contains($localEntity)) {
            $this->localEntities[] = $localEntity;
            $localEntity->setGeneralEntity($this);
        }

        return $this;
    }

    public function removeLocalEntity(LocalEntity $localEntity): self
    {
        if ($this->localEntities->contains($localEntity)) {
            $this->localEntities->removeElement($localEntity);
            // set the owning side to null (unless already changed)
            if ($localEntity->getGeneralEntity() === $this) {
                $localEntity->setGeneralEntity(null);
            }
        }

        return $this;
    }
}
