<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FormationType", inversedBy="formations")
     */
    private $FormationType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FormationLigne", mappedBy="Formation")
     */
    private $formationLignes;

    public function __construct()
    {
        $this->formationLignes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getFormationType(): ?FormationType
    {
        return $this->FormationType;
    }

    public function setFormationType(?FormationType $FormationType): self
    {
        $this->FormationType = $FormationType;

        return $this;
    }

    /**
     * @return Collection|FormationLigne[]
     */
    public function getFormationLignes(): Collection
    {
        return $this->formationLignes;
    }

    public function addFormationLigne(FormationLigne $formationLigne): self
    {
        if (!$this->formationLignes->contains($formationLigne)) {
            $this->formationLignes[] = $formationLigne;
            $formationLigne->setFormation($this);
        }

        return $this;
    }

    public function removeFormationLigne(FormationLigne $formationLigne): self
    {
        if ($this->formationLignes->contains($formationLigne)) {
            $this->formationLignes->removeElement($formationLigne);
            // set the owning side to null (unless already changed)
            if ($formationLigne->getFormation() === $this) {
                $formationLigne->setFormation(null);
            }
        }

        return $this;
    }
}
