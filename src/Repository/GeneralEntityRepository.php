<?php

namespace App\Repository;

use App\Entity\GeneralEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GeneralEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeneralEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeneralEntity[]    findAll()
 * @method GeneralEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeneralEntityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GeneralEntity::class);
    }

    // /**
    //  * @return GeneralEntity[] Returns an array of GeneralEntity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GeneralEntity
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
