<?php

namespace App\Repository;

use App\Entity\LocalEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LocalEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocalEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocalEntity[]    findAll()
 * @method LocalEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalEntityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LocalEntity::class);
    }

    // /**
    //  * @return LocalEntity[] Returns an array of LocalEntity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LocalEntity
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
