<?php

namespace App\Repository;

use App\Entity\FormationType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FormationType|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormationType|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormationType[]    findAll()
 * @method FormationType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormationTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FormationType::class);
    }

    // /**
    //  * @return FormationType[] Returns an array of FormationType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormationType
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
