<?php

namespace App\Repository;

use App\Entity\FormationLigne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FormationLigne|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormationLigne|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormationLigne[]    findAll()
 * @method FormationLigne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormationLigneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FormationLigne::class);
    }

    // /**
    //  * @return FormationLigne[] Returns an array of FormationLigne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FormationLigne
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
