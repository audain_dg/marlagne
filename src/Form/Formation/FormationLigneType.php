<?php

namespace App\Form\Formation;


use App\Entity\Formation;
use App\Entity\FormationLigne;
use App\Entity\FormationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class FormationLigneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('FormationType', EntityType::class, array(
                'class' => FormationType::class,
                'choice_label' => 'name',
                'mapped'      => false,
                'label' => true,

            ))

            ->add('submit', SubmitType::class)
        ;
        $builder->get('FormationType')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $this->addLocalField($form->getParent(),$form->getData());

            }
        );
        /* préremplir pour update*/
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {

                $data = $event->getData();
                /* @var $formation Formation  */
                $formation = $data->getFormation();
                $form = $event->getForm();
                if($formation){
                    $formationType =  $formation->getFormationType();

                    $this->addLocalField($form,$formationType);
                    $form->get('FormationType')->setData($formationType);
                }else{
                    $this->addLocalField($form,null);
                }

            }
        );
    }
    /**
     *
     * @param FormInterface $form
     * @param FormationType|null $formationType
     */
    private function addLocalField(FormInterface $form, ?FormationType $formationType)
    {
        $builder= $form->getConfig()->getFormFactory()->createNamedBuilder(
            'Formation',
            EntityType::class,
            null,
            [
                'class'       => Formation::class,
                'placeholder' => $formationType?'Select your Formation':'You must first select your FormationType',
                'mapped'      => false,
                'required'    => false,
                'choice_label' => 'name',
                'label' => true,
                'auto_initialize' => false,
                'choices' =>$formationType?$formationType->getFormations():[]
            ]
        );

        $form->add($builder->getForm());
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FormationLigne::class,

        ]);
    }
}
