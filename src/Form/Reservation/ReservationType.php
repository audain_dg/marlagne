<?php

namespace App\Form\Reservation;


use App\Entity\Calendar;
use App\Entity\FormationLigne;
use App\Entity\FormationType;
use App\Entity\Reservation;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Date;


class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('Calendar', EntityType::class, array(
                'class' => Calendar::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s');
                },
                'choice_label'  => function (Calendar $calendar) {
                    return
                        $calendar->getDate()->format('d/m/Y') ;
                },
                'placeholder' => 'Votre choix',
            ))
            ->add('dinner', ChoiceType::class,[
                'placeholder' => 'choose',
                'choices' => array(
                    'Oui' => 'Oui',
                    'Non' => 'Non'
                )
            ])
            ->add('supper', ChoiceType::class,[
                'placeholder' => 'choose',
                'choices' => array(
                    'Oui' => 'Oui',
                    'Non' => 'Non'
                )
            ])
            ->add('logement', ChoiceType::class,[
                'placeholder' => 'choose',
                'choices' => array(
                    'Oui' => 'Oui',
                    'Non' => 'Non'
                )
            ])
            ->add('submit', SubmitType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,

        ]);
    }
}
