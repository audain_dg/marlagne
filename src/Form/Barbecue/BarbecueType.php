<?php

namespace App\Form\Barbecue;


use App\Entity\Barbecue;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Date;


class BarbecueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('participation', ChoiceType::class,[
                'placeholder' => 'Choisir',
                'choices' => array(
                    'Oui' => 'Oui',
                    'Non' => 'Non'

                )
            ])
            ->add('numberOfPeople')
            ->add('diet', ChoiceType::class,[
                'placeholder' => 'Choisir',
                'choices' => array(
                    'Classique' => 'Classique',
                    'Végatarien (avec poisson) ' => 'Végatarien (avec poisson)',
                    'Végatarien (sans poisson)' => 'Végatarien (sans poisson)'
                )
            ])
            ->add('allergies',TextareaType::class)
            ->add('submit', SubmitType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Barbecue::class,
        ]);
    }
}
