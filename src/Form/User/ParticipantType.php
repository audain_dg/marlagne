<?php

namespace App\Form\User;

use App\Entity\GeneralEntity;
use App\Entity\LocalEntity;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
class ParticipantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('roles', ChoiceType::class, array(
                    'choices' =>
                        array
                        (
                            'Admin' => 'ROLE_ADMIN',
                            'User' => 'ROLE_USER',
                            'Volontaire'=>'ROLE_VOLONTAIRE',
                            'Salarié(e)'=>'ROLE_Salarie',
                            'Animateur Relais BEPS'=>'ROLE_AnimateurRelaisBeps',
                        ) ,
                    'multiple' => true,
                    'required' => true,
                )
            )
            ->add('firstName')
            ->add('lastName')
            ->add('gender', ChoiceType::class,[
                'placeholder' => 'choose',
                'choices' => array(
                    'Male' => 'h',
                    'Female' => 'f'
                )
            ])
            ->add('phone')
            ->add('birthDate', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
            ])
            ->add('GeneralEntity', EntityType::class, array(
                'class' => GeneralEntity::class,
                'placeholder' => 'choose',
                'choice_label' => 'name',
                'label' => true,
                'mapped'      => false

            ))
            ->add('anecdote')
            ->add('submit', SubmitType::class)

        ;
        $builder->get('GeneralEntity')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                $form = $event->getForm();
                $this->addLocalField($form->getParent(),$form->getData());

            }
        );
        /* préremplir pour update*/
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) {

                $data = $event->getData();
                /* @var $localEntity LocalEntity  */

                $localEntity = $data->getLocalEntity();
                $form = $event->getForm();
                if($localEntity){
                    $generalEntity =  $localEntity->getGeneralEntity();

                    $this->addLocalField($form,$generalEntity);
                    $form->get('GeneralEntity')->setData($generalEntity);

                }else{
                    $this->addLocalField($form,null);
                }

            }
        );
    }
    /**
     *
     * @param FormInterface $form
     * @param GeneralEntity|null $generalEntity
     */
    private function addLocalField(FormInterface $form, ?GeneralEntity $generalEntity)
    {
        $builder= $form->getConfig()->getFormFactory()->createNamedBuilder(
            'LocalEntity',
            EntityType::class,
            null,
            [
                'class'       => LocalEntity::class,
                'placeholder' => $generalEntity?'Select your LocalEntity':'You must first select your GeneralEntity',
                'mapped'      => false,
                'required'    => false,
                'choice_label' => 'name',
                'label' => true,
                'auto_initialize' => false,
                'choices' =>$generalEntity?$generalEntity->getLocalEntities():[]
            ]
        );

        $form->add($builder->getForm());
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
