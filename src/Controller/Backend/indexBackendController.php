<?php
namespace App\Controller\Backend;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class indexBackendController extends AbstractController
{

    /**
     * @Route("/admin", name="admin")
     */
    public function homeBackend(){

        return $this->render('backend/body/index.html.twig');
    }
}