<?php

namespace App\Controller\Backend\Formation;

use App\Entity\FormationLigne;
use App\Form\Formation\FormationType;
use App\Repository\FormationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/formation_ligne")
 */
class FormationLigneController extends AbstractController
{
    /**
     * @Route("/{user_id}/{formation_id}", name="admin_formation_Ligne_delete")
     */
    public function delete(Request $request,$user_id,$formation_id): Response
    {
        $repository = $this->getDoctrine()->getRepository(FormationLigne::class);
        $query_formationLigne = $repository->findOneBy(
            ['User' => $user_id,
              'Formation'=>$formation_id
            ]
        );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($query_formationLigne);
            $entityManager->flush();

        return $this->redirectToRoute('admin_user_edit', [
            'id' => $user_id,
        ]);    }
}