<?php

namespace App\Controller\Backend\Formation;

use App\Entity\FormationType;
use App\Form\Formation\FormationTypeType;
use App\Repository\FormationTypeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/formation_type")
 */
class FormationTypeController extends AbstractController
{
    /**
     * @Route("/", name="admin_formation_type_index", methods={"GET"})
     */
    public function index(FormationTypeRepository $formationTypeRepository): Response
    {
        return $this->render('backend/body/formation_type/index.html.twig', [
            'formation_types' => $formationTypeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_formation_type_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $formationType = new FormationType();
        $form = $this->createForm(FormationTypeType::class, $formationType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            date_default_timezone_set('Europe/Brussels');
            $formationType->setCreatedAt(new \DateTime());
            $entityManager->persist($formationType);
            $entityManager->flush();

            return $this->redirectToRoute('admin_formation_type_index');
        }

        return $this->render('backend/body/formation_type/new.html.twig', [
            'formation_type' => $formationType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_formation_type_show", methods={"GET"})
     */
    public function show(FormationType $formationType): Response
    {
        return $this->render('backend/body/formation_type/show.html.twig', [
            'formation_type' => $formationType,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_formation_type_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FormationType $formationType): Response
    {
        $form = $this->createForm(FormationTypeType::class, $formationType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            date_default_timezone_set('Europe/Brussels');
            $formationType->setUpdatedAt(new \DateTime());

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_formation_type_index', [
                'id' => $formationType->getId(),
            ]);
        }

        return $this->render('backend/body/formation_type/edit.html.twig', [
            'formation_type' => $formationType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_formation_type_delete", methods={"DELETE"})
     */
    public function delete(Request $request, FormationType $formationType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$formationType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($formationType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_formation_type_index');
    }
}
