<?php

namespace App\Controller\Backend\User;

use App\Entity\Barbecue;
use App\Entity\FormationLigne;
use App\Entity\Reservation;
use App\Entity\User;
use App\Form\Barbecue\BarbecueType;
use App\Form\Formation\FormationLigneType;
use App\Form\Reservation\ReservationType;
use App\Form\User\UserType;
use App\Form\User\UserPasswordType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="admin_user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('backend/body/user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->render('backend/body/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user,UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        date_default_timezone_set('Europe/Brussels');
        if ($form->isSubmitted() && $form->isValid()) {
            $session = new Session();
            $session->getFlashBag()->add('toast-success', 'Modification réussie');
            if(!$request->isXmlHttpRequest() && $form->get('LocalEntity')->getData() != NULL) {
                $user->setLocalEntity($form->get('LocalEntity')->getData());
                $user->setUpdatedAt(new \DateTime());
                $this->getDoctrine()->getManager()->flush();

            }


            /* return $this->redirectToRoute('admin_user_index', [
                 'id' => $user->getId(),
             ]);*/
        }
        $userPasswordForm = $this->createForm(UserPasswordType::class, $user);
        $userPasswordForm->handleRequest($request);
        if ($userPasswordForm->isSubmitted() && $userPasswordForm->isValid()) {
            $password = $passwordEncoder->encodePassword($user,$user->getPlainPassword());
            $user->setPassword($password);
            $user->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();
            $session = new Session();
            $session->getFlashBag()->add('toast-success', 'Modification réussie');
            //return $this->redirectToRoute('admin_user_edit',  ['id'=>$user->getId()  ]);

        }
        $formationLigne = new FormationLigne();
        $form_formationLigne = $this->createForm(FormationLigneType::class, $formationLigne);
        $form_formationLigne->handleRequest($request);

       if ($form_formationLigne->isSubmitted() && $form_formationLigne->isValid()) {
           if($request->isXmlHttpRequest() && $form_formationLigne->get('Formation')->getData() != NULL) {
               $entityManager = $this->getDoctrine()->getManager();
               $formationLigne->setUser($user);
               $formationLigne->setFormation($form_formationLigne->get('Formation')->getData());
               $user->setUpdatedAt(new \DateTime());
               $entityManager->persist($formationLigne);
               $entityManager->flush();
           }
        }
        $repository = $this->getDoctrine()->getRepository(FormationLigne::class);
        $query_formationLigne = $repository->findBy(
            ['User' => $user->getId()]
        );

        $reservation = new Reservation();
        $form_reservation = $this->createForm(ReservationType::class,$reservation);
        $form_reservation->handleRequest($request);
        if ($form_reservation->isSubmitted() && $form_reservation->isValid()) {
            if($request->isXmlHttpRequest() && $form_reservation->get('Calendar')->getData() != NULL) {
                $entityManager = $this->getDoctrine()->getManager();
                $reservation->setUser($user);
                $reservation->setCalendar($form_reservation->get('Calendar')->getData());
                $reservation->setDinner($form_reservation->get('dinner')->getData());
                $reservation->setSupper($form_reservation->get('supper')->getData());
                $reservation->setLogement($form_reservation->get('logement')->getData());
                $user->setUpdatedAt(new \DateTime());
                $entityManager->persist($reservation);
                $entityManager->flush();
            }
        }
        $repository = $this->getDoctrine()->getRepository(Reservation::class);
        $query_reservation = $repository->findBy(
            ['User' => $user->getId()]
        );

        $barbecue = new Barbecue();
        $form_barbecue = $this->createForm(BarbecueType::class, $barbecue);
        $form_barbecue->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();
        if ($form_barbecue->isSubmitted() && $form_barbecue->isValid()) {

            if( $user->getBarbecue() == NULL){
                 $entityManager->persist($barbecue);
                $user->setBarbecue($barbecue);
                $user->setUpdatedAt(new \DateTime());
                $entityManager->flush();
                $session = new Session();
                $session->getFlashBag()->add('toast-success', 'Modification réussie');
            }else{
                $query_bbq = $entityManager->getRepository(Barbecue::class )->find($user->getBarbecue()->getId());

                $query_bbq->setParticipation($form_barbecue->get('participation')->getData());
                $query_bbq->setDiet($form_barbecue->get('diet')->getData());
                $query_bbq->setAllergies($form_barbecue->get('allergies')->getData());
                $query_bbq->setNumberOfPeople($form_barbecue->get('numberOfPeople')->getData());
                $user->setUpdatedAt(new \DateTime());
                $entityManager->flush();
            }

        }

        return $this->render('backend/body/user/edit.html.twig', [
            'user' => $user,
            'userPasswordForm' => $userPasswordForm->createView(),
            'userInfoPublicForm' => $form->createView(),
            'form_formationLigne'=>$form_formationLigne->createView(),
            'ListformationLigne'=>$query_formationLigne,
            'form_reservation'=>$form_reservation->createView(),
            'Listreservation'=>$query_reservation,
            'form_barbecue'=>$form_barbecue->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="admin_user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_user_index');
    }
}
