<?php
namespace App\Controller\Frontend;

use App\Entity\Barbecue;
use App\Entity\Calendar;
use App\Entity\Commitment;
use App\Entity\Formation;
use App\Entity\FormationLigne;
use App\Entity\LocalEntity;
use App\Entity\Reservation;
use App\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
class validationFormController extends AbstractController
{

    /**
     * @Route("/validationForm", name="validationForm",methods={"GET","POST"})
     */
    public function homeFrontend(Request $request){
        dump($request);
        $nom = $request->get("Nom");
        $prenom = $request->get("Prénom");
        $DateDeNaissance = $request->get("Date_de_naissance");
        $genre = $request->get("Genre");
        $tel = $request->get("Tel");
        $email = $request->get("Email");
        $matricule = $request->get("Matricule");
        $statut = $request->get("Statut");
        $entiteLocale = $request->get("Entité_Locale");
        $formations= $request->get("Formations");
        $dates = $request->get("calendar");
        $barbecue = $request->get("Barbecue");
        $nbaccompagants = $request->get("Accompagnants");
        $regimeAlimentaire= $request->get("Régime_alimentaire");
        $allergies = $request->get("Allergies");
        $suggestions = $request->get("Suggestions");
        $agreement = $request->get("agreement");

        $user = new User();
        date_default_timezone_set('Europe/Brussels');
        $user->setCreatedAt(new \DateTime());
        $user->setFirstName($nom);
        $user->setLastName($prenom);
        $user->setBirthDate($date = new \DateTime('@'.strtotime($DateDeNaissance)));
        $user->setGender($genre);
        $user->setPhone($tel);
        $user->setEmail($email);
        $user->setMatricule($matricule);
        $repository = $this->getDoctrine()->getRepository(LocalEntity::class);
        $localentityObj = $repository->find($entiteLocale);
        dump($localentityObj);
        $repository = $this->getDoctrine()->getRepository(Commitment::class);
        $comminitment= $repository->find(1);
        $user->setCommitment($comminitment);
        $user->setLocalEntity($localentityObj);
        $user->setRoles( array('ROLE_USER,ROLE_'.strtoupper($statut))); // à modifier
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
        if(isset($formations)){
            foreach ($formations as $formation){
                $entityManager = $this->getDoctrine()->getManager();
                $formationLigne = new FormationLigne();
                $formationLigne->setUser($user);

                $repository = $this->getDoctrine()->getRepository(Formation::class);
                $forlationObj = $repository->find($formation);
                $formationLigne->setFormation($forlationObj);
                $entityManager->persist($formationLigne);
                $entityManager->flush();
            }
        }

        /* Rajout des dates*/
        if(isset($dates)){
            foreach ($dates as $date){
                $partDate= explode(" ", $date);
                $repository = $this->getDoctrine()->getRepository(Calendar::class);
                $dateObj = $repository->find($partDate[0]);

                $repository = $this->getDoctrine()->getRepository(Reservation::class);
                $reservation_query= $repository->findOneBy(['Calendar'=>$dateObj,'User'=>$user]);

                $entityManager = $this->getDoctrine()->getManager();
                $reservation = new Reservation();

                if($reservation_query == NULL){
                    $reservation->setUser($user);
                    $reservation->setCalendar($dateObj);
                    $entityManager->persist($reservation);
                    $entityManager->flush();
                }

            }
            foreach ($dates as $date){
                $partDate= explode(" ", $date);
                $repository = $this->getDoctrine()->getRepository(Calendar::class);
                $dateObj = $repository->find($partDate[0]);

                $repository = $this->getDoctrine()->getRepository(Reservation::class);
                $reservation_query= $repository->findOneBy(['Calendar'=>$dateObj,'User'=>$user]);
                $entityManager = $this->getDoctrine()->getManager();

                if($partDate[1] == "dinner"){
                    $reservation_query->setDinner("Oui");
                    $entityManager->flush();
                }
                if($partDate[1] == "supper"){
                    $reservation_query->setSupper("Oui");
                    $entityManager->flush();
                }
                if($partDate[1] == "logement"){
                    $reservation_query->setLogement("Oui");
                    $entityManager->flush();
                }

            }

            foreach ($dates as $date){
                $partDate= explode(" ", $date);
                $repository = $this->getDoctrine()->getRepository(Calendar::class);
                $dateObj = $repository->find($partDate[0]);

                $repository = $this->getDoctrine()->getRepository(Reservation::class);
                $reservation_query= $repository->findOneBy(['Calendar'=>$dateObj,'User'=>$user]);
                $entityManager = $this->getDoctrine()->getManager();

                if($reservation_query->getDinner() == NULL){
                    $reservation_query->setDinner("Non");
                    $entityManager->flush();
                }
                if($reservation_query->getSupper() == NULL){
                    $reservation_query->setSupper("Non");
                    $entityManager->flush();
                }
                if($reservation_query->getLogement() == NULL){
                    $reservation_query->setLogement("Non");
                    $entityManager->flush();
                }

            }
        }

        $bbq = new Barbecue();
        $bbq->setParticipation($barbecue);
        $bbq->setDiet($regimeAlimentaire[0]);
        $bbq->setNumberOfPeople($nbaccompagants);
        $bbq->setAllergies($allergies);
        $entityManager = $this->getDoctrine()->getManager();
        $user->setBarbecue($bbq);
        $entityManager->persist($bbq);
        $entityManager->flush();

        //return $this->render('frontend/body/index.html.twig');
    }
}