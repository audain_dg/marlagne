<?php
namespace App\Controller\Frontend;

use App\Repository\CalendarRepository;
use App\Repository\FormationTypeRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Repository\FormationRepository;

class indexFrontendController extends AbstractController
{

    /**
     * @Route("/", name="home")
     */
    public function homeFrontend(CalendarRepository $calendarRepository ,FormationRepository $formationRepository,FormationTypeRepository $formationTypeRepository){

        return $this->render('frontend/body/index.html.twig', [
            'formations' => $formationRepository->findAll(),
            'formationTypes'=>$formationTypeRepository->findAll(),
            'calendars'=>$calendarRepository->findAll()
        ]);
    }
}