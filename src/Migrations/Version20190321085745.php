<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190321085745 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE barbecue DROP anecdote, CHANGE diet diet VARCHAR(255) DEFAULT NULL, CHANGE allergies allergies LONGTEXT DEFAULT NULL, CHANGE number_of_people number_of_people VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE barbecue ADD anecdote LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE diet diet VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE allergies allergies LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE number_of_people number_of_people VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
