<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190321084318 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD barbecue_id INT DEFAULT NULL, ADD commitment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649E2A5D7D4 FOREIGN KEY (barbecue_id) REFERENCES barbecue (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649680FAE08 FOREIGN KEY (commitment_id) REFERENCES commitment (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649E2A5D7D4 ON user (barbecue_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649680FAE08 ON user (commitment_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649E2A5D7D4');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649680FAE08');
        $this->addSql('DROP INDEX IDX_8D93D649E2A5D7D4 ON user');
        $this->addSql('DROP INDEX IDX_8D93D649680FAE08 ON user');
        $this->addSql('ALTER TABLE user DROP barbecue_id, DROP commitment_id');
    }
}
