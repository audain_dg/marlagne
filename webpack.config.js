var Encore = require('@symfony/webpack-encore');

Encore

// directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')

    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */

    .addEntry('admin', './assets/js/admin.js')
    .addEntry('adminUserIndex','./assets/js/admin/user/index.js')
    .addEntry('adminUserEdit','./assets/js/admin/user/edit.js')
    .addEntry('adminUserNew','./assets/js/admin/user/new.js')
    .addEntry('adminUserShow','./assets/js/admin/user/show.js')

    .addEntry('adminFormationIndex','./assets/js/admin/formation/index.js')
    .addEntry('adminFormationEdit','./assets/js/admin/formation/edit.js')
    .addEntry('adminFormationNew','./assets/js/admin/formation/new.js')
    .addEntry('adminFormationShow','./assets/js/admin/formation/show.js')

    .addEntry('adminFormationTypeIndex','./assets/js/admin/formationType/index.js')
    .addEntry('adminFormationTypeEdit','./assets/js/admin/formationType/edit.js')
    .addEntry('adminFormationTypeNew','./assets/js/admin/formationType/new.js')
    .addEntry('adminFormationTypeShow','./assets/js/admin/formationType/show.js')

    .addEntry('adminCalendarIndex','./assets/js/admin/calendar/index.js')
    .addEntry('adminCalendarEdit','./assets/js/admin/calendar/edit.js')
    .addEntry('adminCalendarNew','./assets/js/admin/calendar/new.js')
    .addEntry('adminCalendarShow','./assets/js/admin/calendar/show.js')
    //.addStyleEntry('bootstrap','./assets/scss/app.scss')
    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    .enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    .autoProvidejQuery()

// uncomment if you use API Platform Admin (composer req api-admin)
//.enableReactPreset()
//.addEntry('admin', './assets/js/admin.js')
;
var config = Encore.getWebpackConfig();

config.module.rules.unshift({
    parser: {
        amd: false
    }
});
module.exports = config;

